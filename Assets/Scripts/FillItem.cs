﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillItem : MonoBehaviour
{

    public Image myCoffee;
    public GameObject mySmoke;
    public Image myText;
    public float coffeeDeltaValue;
    public float smokeDeltaValue;
    public float textDeltaValue;
    public float coffeeLagValue;
    public float smokeLagValue;
    //public float maxValue;
    public float filledPosition, emptyPosition;

    [Header("Fill Settings")]
    public bool leftClickControl;
    public bool rightClickControl;
    public bool fillSnap;
    [Header("Empty Settings")]
    public bool EmptySnap;

    private InputHandler _inputhandler;

    void Start()
    {
        _inputhandler = InputHandler.Instance();

        if (rightClickControl)
        {
            _inputhandler.OnRightClick += HandleClick;
        }
        else if (leftClickControl)
        {
            _inputhandler.OnLeftClick += HandleClick;
        }

        _inputhandler.OnMiddleClick += HandleMiddleClick;



    }


    IEnumerator Empty()
    {
        if (!EmptySnap)
        {
            for (float y = myCoffee.rectTransform.localPosition.y; y >= emptyPosition; y -= coffeeDeltaValue)
            {
                myCoffee.rectTransform.localPosition = new Vector3(myCoffee.transform.localPosition.x, y, myCoffee.transform.localPosition.z);
                //myImage.fillAmount = y;
                yield return new WaitForSeconds(coffeeLagValue);
            }

            //for (float f = mySmoke.fillAmount; f >= 0f; f -= smokeDeltaValue)
            //{
            //    mySmoke.fillAmount = f;
            //    yield return null;
            //}
            mySmoke.SetActive(false);

            for (float a = myText.color.a; a >= 0f; a -= textDeltaValue)
            {
                myText.color = new Color(myText.color.r, myText.color.g, myText.color.b, a);
                yield return null;
            }
        }
        else
        {
            myCoffee.rectTransform.localPosition = new Vector3(myCoffee.transform.localPosition.x, emptyPosition, myCoffee.transform.localPosition.z);
            mySmoke.SetActive(false);
            myText.color = new Color(myText.color.r, myText.color.g, myText.color.b, 0f);
        }

    }

    IEnumerator Fill()
    {
        if (!fillSnap)
        {
            for (float y = myCoffee.rectTransform.localPosition.y; y <= filledPosition; y += coffeeDeltaValue)
            {
                myCoffee.rectTransform.localPosition = new Vector3(myCoffee.transform.localPosition.x, y, myCoffee.transform.localPosition.z);
                //myImage.fillAmount = y;
                yield return new WaitForSeconds(coffeeLagValue);
            }

            //for (float f = mySmoke.fillAmount; f <= 1f; f += smokeDeltaValue)
            //{
            //    mySmoke.fillAmount = f;
            //    yield return null;
            //}
            mySmoke.SetActive(true);

            for (float a = myText.color.a; a <= 1f; a += textDeltaValue)
            {
                myText.color = new Color(myText.color.r, myText.color.g, myText.color.b, a);
                yield return null;
            }
        }
        else
        {
            myCoffee.rectTransform.localPosition = new Vector3(myCoffee.transform.localPosition.x, filledPosition, myCoffee.transform.localPosition.z);
            mySmoke.SetActive(true);
            myText.color = new Color(myText.color.r, myText.color.g, myText.color.b, 1f);
        }

    }

    public void HandleClick()
    {
        StopAllCoroutines();
        StartCoroutine("Fill");
    }

    public void HandleMiddleClick()
    {
        StopAllCoroutines();
        StartCoroutine("Empty");
    }
}
