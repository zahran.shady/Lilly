﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour, IPointerClickHandler
{
    //public float incrementValue;
    //public float lagValue;

    public Action OnLeftClick = null;
    public Action OnRightClick = null;
    public Action OnMiddleClick = null;

    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("pointer click");
        //if (eventData.button == PointerEventData.InputButton.Left)
        //{
        //    Debug.Log("left click");

        //    if (targetImage.fillAmount < 1.0f)
        //    {
        //        targetImage.fillAmount += incrementValue;
        //    }

        //}
        //else if (eventData.button == PointerEventData.InputButton.Right)
        //{

        //    Debug.Log("right click");

        //    if (targetImage.fillAmount > 0f)
        //    {
        //        targetImage.fillAmount -= incrementValue;
        //    }
        //}
    }



    private float _holdtimer = 0;
    public bool _canIAuto = true;

    public FillItem cupLarge, cupSmall;
    bool isCupLargeFilling, isCupSmallFilling;

    public float holdTimerThreshold;
    public float autoplayDuration;

    private static InputHandler instance = null;

    public static InputHandler Instance()
    {
        if (instance != null)
        {
            return instance; // return the singleton if the instance has been setup
        }
        else
        { // otherwise return an error
            Debug.LogWarning("InputHandler Not Initialized...");
        }
        return null;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;// if not, give it a reference to this class...
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
    }

    void Update()
    {
        //if i can auto
        if (_canIAuto)
        {
            //left click
            if (Input.GetMouseButtonDown(0))
            {
                if (OnLeftClick != null)
                {
                    OnLeftClick();
                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                if (OnRightClick != null)
                {
                    OnRightClick();
                }
            }

            if (Input.GetMouseButton(2))
            {
                _holdtimer += Time.deltaTime;
                //if (_holdtimer >= 2)
                //{
                //    //invoke auto
                //    //reset timer
                //    _holdtimer = 0;
                //    //toggle auto flag
                //    //_autoflag = false;
                //}
                //if (OnMiddleClick != null)
                //{
                //    OnMiddleClick();
                //}

            }
            else if (Input.GetMouseButtonUp(2))
            {
                if (_holdtimer >= holdTimerThreshold)
                {
                    //reset timer
                    _holdtimer = 0;
                    //toggle enable auto flag
                    _canIAuto = false;
                    //invoke auto
                    initiateAuto();

                }
                else
                {
                    if (OnMiddleClick != null)
                    {
                        OnMiddleClick();
                    }
                }
            }
        }

        else
        {

            //middle click again to go back to normal
            if (Input.GetMouseButtonDown(2))
            {
                //stop auto
                ResetAll();
            }
            
        }



    }

    void initiateAuto()
    {
        Debug.Log("auto started");
        cupLarge.HandleMiddleClick();
        cupSmall.HandleMiddleClick();
        cupLarge.HandleClick();
        cupSmall.HandleClick();
        StartCoroutine("RunAgainAfter", autoplayDuration);
    }

    void ResetAll()
    {
        StopCoroutine("RunAgainAfter");
        cupLarge.HandleMiddleClick();
        cupSmall.HandleMiddleClick();
        _canIAuto = true;
    }

    IEnumerator RunAgainAfter(float fTime)
    {
        Debug.Log("coroutine started");
        yield return new WaitForSeconds(fTime);
        initiateAuto();
    }
}
